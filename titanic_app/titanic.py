from flask import Flask, request, jsonify, render_template
from flask_restful import Api, Resource, reqparse, abort , fields, marshal_with , reqparse
from flask_mysqldb import MySQL
import MySQLdb



app = Flask(__name__)
app.secret_key= " "


#Database configuration settings 
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'siachras'
app.config['MYSQL_PASSWORD'] = 'bbb21567'
app.config['MYSQL_DB'] = 'titanic'

#Initialiaze the database
db = MySQL(app)

#This is for POST at /people to receive the json
people_put_args = reqparse.RequestParser()
people_put_args.add_argument("survived", type=str , help="Give a boolean Value as survived entry")
people_put_args.add_argument("passengerClass", type=int, help="Give a integer Value as passengerClass entry")
people_put_args.add_argument("name", type=str, help="Give a str Value as name entry")
people_put_args.add_argument("sex", type=str, help="Give a str Value as sex entry")
people_put_args.add_argument("age", type=int, help="Give a integer Value as age entry")
people_put_args.add_argument("siblingsOrSpousesAboard", type=int, help="Give a integer Value as siblingsOrSpousesAboard entry")
people_put_args.add_argument("parentsOrChildrenAboard", type=int, help="Give a integer Value as parentsOrChildrenAboard entry")
people_put_args.add_argument("fare", type=float, help="Give a float Value as fare entry")


@app.route('/')
def index():
	return render_template('index.html')


@app.route('/people', methods=['GET' , 'POST'])
def show():
	if request.method == 'GET':
		cursor = db.connection.cursor(MySQLdb.cursors.DictCursor)
		cursor.execute("SELECT uuid,survived,pclass,name,sex,age,siblings_spouse_aboard,parents_children_aboard, FLOOR(fare) as fare from titanic;")
		info=cursor.fetchall()
		return jsonify(info)
	elif request.method == 'POST':
		userDetails = people_put_args.parse_args()
		if userDetails['survived'] == bool:
			survived = 1;
		else:
			survived = 0;
		#survived = userDetails['survived']
		pclass = userDetails['passengerClass']
		name = userDetails['name'] 
		sex = userDetails['sex']
		age = userDetails['age']
		siblings_spouse_aboard = userDetails['siblingsOrSpousesAboard']
		parents_children_aboard = userDetails['parentsOrChildrenAboard']
		fare = userDetails['fare']
		cursor = db.connection.cursor(MySQLdb.cursors.DictCursor)
		#taking the last value of uuid of database
		last_uuid = cursor.execute("SELECT uuid FROM titanic ORDER BY uuid desc limit 1;")
		uuid = cursor.fetchall()
		#this returns tuple , so i change this to list so i can modify its value.
		list_uuid=list(uuid)
		last_uuid=list_uuid[0]
		int_uuid=(last_uuid['uuid']) + 1 
		cursor.execute("INSERT INTO titanic (uuid,survived,pclass,name,sex,age,siblings_spouse_aboard,parents_children_aboard,fare) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s);", (int_uuid, survived, pclass, name, sex, age, siblings_spouse_aboard, parents_children_aboard, fare))
		db.connection.commit()
		cursor.close()
		#return(last_uuid['uuid'])
		return "success of import"

	else:
		return "Unknown Method , please use GET OR POST"

@app.route('/people/<uuid>', methods=['GET' , 'DELETE' , 'PUT'])
def people_uuid(uuid):
	if request.method == 'GET':
		int_uuid=int(uuid)
		cursor = db.connection.cursor()
		#cursor = db.connection.cursor(MySQLdb.cursors.DictCursor)
		#sql_query = "SELECT uuid from titanic WHERE uuid = %s "
		#cursor.execute(sql_query, (int_uuid,))
		cursor.execute("SELECT uuid,survived,pclass,name,sex,age,siblings_spouse_aboard,parents_children_aboard, FLOOR(fare) as fare from titanic WHERE uuid = %s", int_uuid)
		r = [dict((cursor.description[i][0], value)
        for i, value in enumerate(row)) for row in cursor.fetchall()]
		return jsonify({'results': r})

	elif request.method == 'DELETE':
		return render_template('index.html')
		#TODO 
	elif request.method == 'PUT':
		int_uuid=int(uuid)
		userDetails = people_put_args.parse_args()
		if userDetails['survived'] == bool:
			survived = 1;
		else:
			survived = 0;
		pclass = userDetails['passengerClass']
		name = userDetails['name'] 
		sex = userDetails['sex']
		age = userDetails['age']
		siblings_spouse_aboard = userDetails['siblingsOrSpousesAboard']
		parents_children_aboard = userDetails['parentsOrChildrenAboard']
		fare = userDetails['fare']
		cursor = db.connection.cursor(MySQLdb.cursors.DictCursor)
		#TO FIX THIS
		#cursor.execute("UPDATE titanic SET (uuid = %s ,survived = %s ,pclass = %s ,name = %s,sex = %s ,age = %s ,siblings_spouse_aboard = %s,parents_children_aboard = %s,fare = %s);", (int_uuid, survived, pclass, name, sex, age, siblings_spouse_aboard, parents_children_aboard, fare))
		db.connection.commit()
		cursor.close()
		return"success of import"
#api.add_resource(people, "/people/")
#api.add_resource(peopleuuid, "/people/<int:uuid>")

if __name__ == "__main__":
	app.run(debug=True)
